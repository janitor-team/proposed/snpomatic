= findknownsnps(1)
:doctype: manpage
:man manual: SNP-o-matic Manual
:man source: SNP-o-matic 1.0
:page-layout: base

== NAME

findknownsnps - main executable for snpomatic

== SYNOPSIS

*findknownsnps* <options>

== DESCRIPTION

findknownsnps is the main executable for the **snpomatic** software.

== OPTIONS

These options control whether output is written to file(s), standard output, or
directly to a man pager.

  *--genome=GENOME_FILE*::
      FASTA file with chromosomes (mandatory)

  *--fasta=FASTA_FILE*::
    FASTA file with Solexa reads (mandatory, except when --fastq or --index is
    used)

  *--fasta=FASTA_FILE*::
    FASTA file with Solexa reads (mandatory, except when --fastq or --index is
    used)

  *--fastq=FASTQ_FILE*::
    FASTQ file with Solexa reads (mandatory, except when --fasta or --index is
    used)

  *--fastq2=FASTQ_FILE2*::
    FASTQ file with Solexa reads (optional; read mate)

  *--nono=FILENAME*::
    File with list of read names (!) to ignore (optional)

  *--regions=REGION_FILE*::
    Region file for finding new SNPs (optional) [DEPRECATED]

  *--snps=SNP_FILE*::
    Simple SNP file (optional)

  *--gff=GFF_FILE*::
    GFF file with SNPs (optional)

  *--uniqueness=FILE*::
    Output a uniqueness data file for the reference; no Solexa reads needed;
    implies--noshortcuts` (optional)

  *--pileup=FILE*::
    Outputs complete pileup into that file (optional)

  *--cigar=FILE*::
    Outputs alignment in CIGAR format (optional)

  *--gffout=FILE*::
    Outputs reads in GFF format (optional)

  *--coverage=FILENAME*::
    Outputs (high depth) coverage data (optional)

  *--wobble=FILENAME*::
    Outputs a list of possible variations (optional; paired reads only)
    [UNDER CONSTRUCTION]

  *--fragmentplot=FILENAME*::
    Outputs a plot of fragment size distribution to a file (optional)

  *--snpsinreads=FILENAME*::
    Outputs a list of reads containing known SNPs to a file (optional)

  *--indelplot=FILENAME*::
    Outputs indel data to a file (optional)

  *--inversions=FILENAME*::
    For paired reads, writes read matches indicating inversions into a file (optional)

  *--faceaway=FILENAME*::
    For paired reads, writes read matches that "face away" from each other into a file (optional)

  *--sqlite=FILENAME*::
    Creates a sqlite text file with alignment data [EXPERIMENTAL] (optional)

  *--sam=FILENAME*::
    Creates a SAM alignment file (optional)

  *--spancontigs=FILENAME*::
    Outputs read pairs where "half" reads map uniquely to different
    contigs (optional)

  *--bins=FILE_PREFIX*::
    Outputs no-match, single-match and multi-match Solexa reads into prefixed
    files (optional)

  *--binmask=MASK*::
    Mask of 1s and 0s to turn off individual bins. Order: No match, single
    match, multi-match, IUPAC. Example: 0100 creates only single-match bin.
    (optional; default:1111)

  *--pair=NUMBER*::
    For paired reads, the length of the first part of the read (mandatory for
    paired reads)

  *--fragment=NUMBER*::
    For paired reads, the average fragment length (mandatory for paired reads)

  *--variance=NUMBER*::
    For paired reads, the variance of the fragment length to either side (optional; default: 1/4 of fragment size)

  *--wobblemax=NUMBER*::
    Maximum number of mismatches for wobble (optional; default 2; see --wobble)

  *--mspi=NUMBER*::
    Maximum number of SNPs per chromosomal index (optional; default:8)

  *--index=FILENAME*::
    Index filename (index will be created if it does not exist; optional)

  *--noshortcuts*::
    Will process all chrososomal regions, even those with lots'o'repeats (optional;
    no value)

  *--snpsonly*::
    Only lists found SNPs in the pileup (optional; no value)

  *--chromosome=NAME*::
    Discards all chromosomes but NAME prior to run (optional)

  *--index_from=NUMBER*::
    Starts indexing at this position on all chromosomes (optional)

  *--index_to=NUMBER*::
    Stops indexing at this position on all chromosomes (optional)

  *--chop=NUMBER*::
    For paired reads, if one but not the other matches, shorten the other by
   NUMBER` bases (optional)

  *--index1=NUMBER`*::
    Length of internal index 1 (optional; default:10)

  *--index2=NUMBER*::
    Length of internal index 2 (optional; default:16)

  *--memory_save=NUMBER*::
    Indexes the genome every NUMBER of positions; saves memory and runtime, but
    can have strange side effects (optional)

  *--multimatch*::
    Puts a multiple-matching read to a random position (optional) [currently
    paired reads only]

  *--singlematch*::
    Only performs additional output functions for single matches (optional)
    [currently paired reads only]

  *--foum*::
    For paired reads, at least one read has to match uniquely in the genome
    (force one unique match) (optional)

  *--mismatch*::
    The number of mismatches allowed outside the index (index1+index2)
    (optional)

  *--rpa=FILENAME*::
    Writes all read pair alignments into a file (optional)
